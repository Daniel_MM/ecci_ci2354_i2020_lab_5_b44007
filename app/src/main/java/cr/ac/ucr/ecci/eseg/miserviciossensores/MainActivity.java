package cr.ac.ucr.ecci.eseg.miserviciossensores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements DownloadCallback {

    public static final String TAG_IMG = "IMG";

    // Compomentes de la interfaz
    private ProgressDialog mDialog;
    private WebView mWebView;
    private ImageView mImageView;
    private TextView mDataText;


    private NetworkFragment mNetworkFragment;
    // Boolean para tener control si un download esta en progreso y no iniciarlo N veces
    private boolean mDownloading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDataText = findViewById(R.id.text);
        mWebView = findViewById(R.id.webView);
        mImageView = findViewById(R.id.imageView);

        mDataText.setVisibility(View.INVISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

    }


    // Menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.example_one:
                // Mostramos un dialogo de barra de progreso mediante una tarea
                ejemploBarraProgreso();
                return true;
            case R.id.example_two:
                // Mostramos una vista web mediante una tarea
                ejemploVistaWeb();
                return true;
            case R.id.example_three:
                // Mostramos una imagen mediante una tarea
                ejemploMostrarImagen();
                return true;
            case R.id.example_four_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
                startFragmentTask("https://www.facebook.com");
                return true;
            case R.id.example_five_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos
                // Este devuelve un html a partir del servicion REST de Google
                startFragmentTask("https://www.google.com/search?q=universidad+costa+rica");
                return true;
            case R.id.example_fetch_action:
                // Cuando el usuario hace clic en extraer, se obtiene los primeros N caracteres del HTML
                // Iniciamos el download de la página
                startDownload();
                return true;
            case R.id.example_clear_action:
                // Limpiamos el texto de resultado y cancelamos el download si este aun esta en proceso
                finishDownloading();
                mDataText.setText(R.string.cierre_de_tarea);
                return true;
            case R.id.example_six_init_action:
                // Iniciamos el servicio
                return true;
            case R.id.example_six_clear_action:
                // Detenemos el servicio
                return true;
            case R.id.example_seven_connect_action:
                // Servicio de localizacion
                return true;
            case R.id.example_seven_init_action:
                // Servicio de localizacion
                return true;
            case R.id.example_seven_clear_action:
                // Detener el servicion de localizacion
                return true;
            case R.id.example_seven_disconnect_action:
                // Servicion de localizacion
                return true;
            case R.id.example_eigth_connect_action:
                // Servicion de acelerómetro
                return true;
            case R.id.example_eigth_disconnect_action:
                // Servicion de acelerómetro
                return true;
            case R.id.example_nine_init_action:
                return true;
            case R.id.example_ten_init_action:
                // Servicio Rest
                return true;
        }
        return false;
    }

    //**********************************************************************************************

    private void ejemploBarraProgreso() {
        // Mostramos un dialogo de barra de progreso
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Ejecutando tarea asincrónica...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();
        // Ejecutamos la tarea asincronica y actualizamos la barra de progreso
        new TaskBarraProgreso().execute();
    }

    //**********************************************************************************************

    @SuppressLint("SetJavaScriptEnabled")
    private void ejemploVistaWeb() {
        // Solo visible el web view
        mWebView.setVisibility(View.VISIBLE);
        // configuro que el webview se abra dentro de la app
        mWebView.setWebViewClient(new WebViewClient());
        // opciones de configuracion del webview
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        // Ejecutamos la tarea asincronica para mostrar el contenido de una pagina web
        // mediante un Runnable que permite la ejecucion de tareas ejecutadas por un thread
        // metodo HTTP como POST
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("https://www.google.com");
            }
        });
    }

    //**********************************************************************************************

    private void ejemploMostrarImagen() {
        // Solo visible la imagen
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.VISIBLE);
        // Ejecutamos la tarea asincronica que baja y muestra la imagen con la ruta de descarga
        new TaskMostrarImagen().execute("https://www.ecci.ucr.ac.cr/sites/all/themes/ecci_bootstrap/img/logos/logo-ecci-vertical-blanco.png");
    }


    //**********************************************************************************************
    // Servicio Http download
    private void startFragmentTask(String url) {
// Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
//
        mDataText.setText(url);
// Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
        mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), url);
    }

    private void startDownload() {
// si no estamos bajando contenido y el fragmento con la tarea fue inicializado
        if (!mDownloading && mNetworkFragment != null) {
// Ejecutamos el download en la tarea async
            mNetworkFragment.startDownload();
// Marcamos el download en proceso
            mDownloading = true;
        }
    }

    @Override
    public void finishDownloading() {
// marcamos que finaliza el proceso de download
        mDownloading = false;
// Cancelamos el fragmento con la tarea async
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
// Removemos el fragmento para poder iniciar luego una nueva petición distinta a la anterior
        NetworkFragment networkFragment = (NetworkFragment) getSupportFragmentManager()
                .findFragmentByTag(NetworkFragment.TAG_FRAGMENT);
        if (networkFragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().remove(networkFragment).commit();
            manager.popBackStack();
        }
    }

    // Actualizamos y mostramos los datos resultado del download
    @Override
    public void updateFromDownload(String result) {
        if (result != null) {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        } else {
            mDataText.setText(null);
            Toast.makeText(getApplicationContext(), "Error de Conexion",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Este metodo permite mostrar el estado de la tarea Async download
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch (progressCode) {
// You can add UI behavior for progress updates here.
            case Progress.ERROR:
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                break;
            case Progress.CONNECT_SUCCESS:
                Toast.makeText(getApplicationContext(), "CONNECT_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "GET_INPUT_STREAM_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_IN_PROGRESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    // Informacion sobre el estado de la red
    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
    }

    // Estado de la red, verificar el estado de la red
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
        return (networkInfo != null && networkInfo.isConnected());
    }

    //**********************************************************************************************
    // Clase para la tarea asincronica que muestra la barra de progreso
    // Params -> String -> doInBackground
    // Progress -> Float -> onProgressUpdate
    // Result -> Integer -> onPostExecute
    //TODO: fix possible memory leak
    private class TaskBarraProgreso extends AsyncTask<String, Float, Integer> {
        @Override
        protected void onPreExecute() {
            // antes de ejecutar
            mDialog.setProgress(0);
        }

        @Override
        protected Integer doInBackground(String... params) {
            // Cuando se dispara la tarea simulamos el procesamiento en la tarea asincrnica
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
                // publicamos el progreso de la tarea
                publishProgress(i / 100f);
            }
            return 100;
        }

        protected void onProgressUpdate(Float... values) {
            // actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }

        protected void onPostExecute(Integer bytes) {
            // Cuando la tarea termina cerramos la barra de progreso
            mDialog.dismiss();
        }
    }

    //**********************************************************************************************

    // Clase para la tarea asincronica de Imagen
    //TODO: fix possible memory leak
    private class TaskMostrarImagen extends AsyncTask<String, Void, Bitmap> {

        // La tarea se ejecuta en un thread tomando como parametro el eviado en
        // AsyncTask.execute()
        @Override
        protected Bitmap doInBackground(String... urls) {
            // tomanos el parámetro del execute() y bajamos la imagen
            return loadImageFromNetwork(urls[0]);
        }

        // El resultado de la tarea tiene el archivo de imagen el cual mostramos
        protected void onPostExecute(Bitmap result) {
            mImageView.setImageBitmap(result);
        }

        // metodo para bajar la imagen
        private Bitmap loadImageFromNetwork(String url) {
            try {
                return BitmapFactory.decodeStream(
                        (InputStream) new URL(url).getContent());
            } catch (Exception e) {
                Log.v(TAG_IMG, Objects.requireNonNull(e.getMessage()));
            }
            return null;
        }
    }


}

